# Bawkalm Grammar

The grammar of the constructed language Bawkalm.  Bawkalm is a logical language inspired by Lojban and Toaq.  It's main features are a case system instead of cases, well defined chaining, and a focus on an analytic lexicon over grammar.